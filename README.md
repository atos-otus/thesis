# Пректная работа по DevOps

**Уважаемые преподаватели, сдачу или не сдачу проектной работы оставляем на ваше усмотрение, проект был рабочий, 
но к моменту как все студенты из нашей группы ушли в отпуска, на счету основного аккаунта Google Cloud закончились деньги, 
поэтому этот проект без адаптации перенесли только на бесплатный gitlab.com** 


## План работ:

- [x] Создать новый аккаунт в GCP, подготовить к работе
- [x] Развернуть кластер GKE и GitLab
- [x] В GitLab настроить основной репозиторий и доступ к нему:
  - [x] Основные пользователи с ролью Maintainer
  - [x] Дополнительные пользователи с ролью Developer
- [x] В Kubernetes развернуть наше приложение, проверить работоспообность
- [x] В GitLab настроить CI/CD таким образом, чтобы у пользователей с ролью Developer автоматически выполнялись сборка и
тесты приложения, а также можно было посмотреть собранное приложение (Review App), а пользователи с ролью Maintainer 
могли принимать Merge Request и вручную разворачивать приложение в продуктив 
- [x] Настроить мониторинг (сбор метрик, алертинг, визуализация)

## Создать новый аккаунт в GCP, подготовить к работе

Создан аккаунт в GCP.

Создан проект: 
```bash
$ gcloud auth login <Account Owner>
$ gcloud projects create otus-thesis-245613 --name "Otus Thesis" --set-as-default
```

Для четырех студентов ресурсу проекта otus-thesis-245613 выдана роль roles/editor:
```bash
$ gcloud projects add-iam-policy-binding otus-thesis-245613 --member user:<Chirkov> --role roles/editor
$ gcloud projects add-iam-policy-binding otus-thesis-245613 --member user:<Kuznetsov> --role roles/editor
$ gcloud projects add-iam-policy-binding otus-thesis-245613 --member user:<Lomtev> --role roles/editor
$ gcloud projects add-iam-policy-binding otus-thesis-245613 --member user:<Zyuzgina> --role roles/editor
```

Локально сделаны доп. настройки:
```bash
$ gcloud config set account <Member>
$ gcloud config set compute/region europe-north1
$ gcloud config set compute/zone europe-north1-b
```
## Развернуть кластер GKE и GitLab

Поскольку установка GitLab Omnibus помечена как deprecated, использовалась официальная документация Google по установке 
[production-ready GitLab](https://cloud.google.com/solutions/deploying-production-ready-gitlab-on-gke). Для установки GKE и GitLab 
воспользовались terraform-модулем [terraform-google-gke-gitlab](https://github.com/terraform-google-modules/terraform-google-gke-gitlab).

Для хранения состояния terraform создан bucket:
```bash
$ gsutil mb -l europe-north1 gs://tfstate-otus-thesis-245613
```

Скопирован модуль создания кластера GKE и установки Gitlab:
```bash
$ git clone https://github.com/terraform-google-modules/terraform-google-gke-gitlab.git terraform/modules/terraform-google-gke-gitlab && rm -rf terraform/modules/terraform-google-gke-gitlab/.git
```

Для нашего окружения сделаны правки в файлах: 
```
terraform-google-gke-gitlab/main.tf
terraform-google-gke-gitlab/values.yaml.tpl
terraform-google-gke-gitlab/output.tf
```

**main.tf**

* изменен инстанс для БД PostgeSQL
  ```diff
   settings {
  -   tier            = "db-custom-4-15360"
  +   tier            = "db-custom-1-3840"
      disk_autoresize = true
  ```

* инсталляция Redis сделана без отказоустойчивости и изменён размер выделенной памяти для Redis
  ```diff
  resource "google_redis_instance" "gitlab" {
    name               = "gitlab"
  - tier               = "STANDARD_HA"
  - memory_size_gb     = 5
  + tier               = "BASIC"
  + memory_size_gb     = 2
    region             = "${var.region}"
  ```

* удален alternative_location_id для Redis

  ```diff
    location_id             = "${var.region}-a"
  - alternative_location_id = "${var.region}-f"
    display_name            = "GitLab Redis"

  ```

* уменьшен размер инсансов в кластере GKE
  ```diff
    node_config {
    preemptible  = false
  - machine_type = "n1-standard-4"
  + machine_type = "n1-standard-2"
  ```

* увеличен таймаут для установки GitLab и удалена опция установки точной версия GitLab 1.7.1
  ```diff
    chart      = "gitlab"
  - version    = "1.7.1"
  - timeout    = 600
  + timeout    = 1200
  ```

**values.yaml.tpl**

* Поскольку для одного домена у Let's Encrypt есть лимит на выдачу сертификатов, использоваля собственный домен
  ```diff
  hosts:
  - domain: ${INGRESS_IP}.xip.io
  + domain: linux.technology
    https: true
  ```

* Размер для Gitaly уменьшен
  ```diff
  persistence:
  - size: 200Gi
  + size: 20Gi
    storageClass: "pd-ssd"
  ```

* Включен привелигерованный режим работы контейнеров
  ```diff
  runners:
  + privileged: true
    locked: false
  ```

**output.tf**

* Поправлен вывод адреса GitLab:
  ```diff
    output "gitlab_url" {
  - value       = "https://gitlab.${google_compute_address.gitlab.address}.xip.io"
  + value       = "https://gitlab.linux.technology"
    description = "URL where you can access your GitLab instance"
  ```

Шаги установки:

* Инициализация 

  ```
   $ terraform init terraform/
  ```

* Первоначально установлен ресурс для IP GitLab

  ```
  $ terraform apply -var-file=terraform/terraform.tfvars -target=module.gke-gitlab.google_compute_address.gitlab terraform/
  ```

* Прописана DNS-запись gitlab.linux.technology

* Установен GKE и GitLab

  ```
  $ terraform apply -var-file=terraform/terraform.tfvars terraform/
  ```

Наш GitLab доступен по ссылке: https://gitlab.linux.technology

## В GitLab настроить основной репозиторий и доступ к нему

В GitLab создана группа *atos-otus* с уровнем видимости Public.

Созданы пользователи и добавлены в группу с соответстующей ролью:
* chirkov (Maintainer)
* dev1 (Developer)
* dev2 (Developer)
* kuznetsov (Maintainer)
* lomtev (Maintainer)
* zyuzgina (Maintainer)

В группе *atos-otus* создан проект *thesis* с доступом Public.

Выложен локальный репозитарий:
```
$ git remote add origin https://gitlab.linux.technology/atos-otus/thesis.git
$ git push -u origin master
```

## В Kubernetes развернуть наше приложение, проверить работоспообность

### Тестовая сборка приложения 

Все исходны коды микросервисного приложения лежат в директории src.

Для тестового деплоя приложения были выполнены следующие шаги:
1. Создана сеть
  ```bash
  $ docker network create search-engine 
  ```

2. Запущен микросервис mongodb
  ```bash
  $ docker run --rm -d --name mongodb --network search-engine mongo:3.6
  ```

3. Запущен микросервис rabbitmq с переменными RABBITMQ_DEFAULT_USER и RABBITMQ_DEFAULT_PASS
  ```bash
  $ docker run --rm --hostname rabbitmq --name rabbitmq -e RABBITMQ_DEFAULT_USER=rmquser -e RABBITMQ_DEFAULT_PASS=rmqpass --network search-engine -d rabbitmq:alpine
  ```

4. Создан Dockerfile и собран образ для crawler
  ```bash
  $ cd src/search_engine_crawler && make build
  ```

5. Запущен микросервис crawler 
  ```bash
  $ docker run --rm -d --network search-engine sobones/crawler
  ```

6. Создан Dockerfile и собран образ для crawler
  ```bash
  $ cd src/search_engine_ui && make build
  ```

7. Запущен микросервис ui_crawler 
  ```bash
  $ docker run --rm -d --network search-engine  -p 8000:8000 sobones/ui_crawler
  ```

### Сборка приложения для kubernetes 

Для запуска приложения в kubernetes были собраны пакеты Helm. Сервисы rabbitmq и mongodb устанавливались из репозитория stable. 

Основной пакет находится в директории kubernetes/search-engine, пакеты микросервисов crawler и ui в поддиректории app-charts:
```
kubernetes
├── ns-app.yml
└── search-engine
    ├── Chart.yaml
    ├── app-charts
    │   ├── crawler
    │   │   ├── Chart.yaml
    │   │   ├── templates
    │   │   │   └── deployment.yaml
    │   │   ├── values.yaml
    │   │   └── values.yaml.example
    │   └── ui
    │       ├── Chart.yaml
    │       ├── templates
    │       │   ├── deployment.yaml
    │       │   ├── ingress.yaml
    │       │   └── service.yaml
    │       ├── values.yaml
    │       └── values.yaml.example
    ├── charts
    │   ├── crawler-0.1.0.tgz
    │   ├── mongodb-4.0.5.tgz
    │   ├── rabbitmq-6.1.6.tgz
    │   └── ui-0.1.0.tgz
    ├── requirements.lock
    ├── requirements.yaml
    ├── values.yaml
    └── values.yaml.example
```

Для запуска сервиса используется текущий кластер kubernetes gitlab.

Для тестового ручного запуска создан namespace app:
```bash
$ kubectl apply -f kubernetes/ns-app.yml
```

Установлен сервис:
```bash
$ helm dep update kubernetes/search-engine/
$ helm install kubernetes/search-engine/ --namespace app -n search-engine
```

Удален сервис:
```bash
$ helm delete --purge search-engine
```

## В GitLab настроить CI/CD

Настроен классический pipeline:
![Pipeline](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)

Коммит на не-master ветке запускает три этапа pipeline:
* сборка приложения с тегом названия git-ветки и загрузка образов на Docker Hub
* тестирование этих приложений взятых c Docker Hub
* установка сервиса для Review в kubernetes namespace review (есть ручной job для удаления этого сервиса)

Коммит на master ветке запускает пять этапов pipeline:
* сборка приложения с тегом master и загрузки образов на Docker Hub
* тестирование этих приложений взятых c Docker Hub
* загрузка образов на Docker Hub с тегом версии, взятой из файла VERSION каждого микросервиса (для истории)
* установка сервиса для тестов в kubernetes namespace staging
* ручная установка сервиса в kubernetes namespace production

В Gitlab определены переменные
```
CI_REGISTRY_USER
CI_REGISTRY_PASSWORD
```

а также файлы с параметрами установки сервиса 
```
kubernetes/search-engine/app-charts/crawler/values.yaml
kubernetes/search-engine/app-charts/ui/values.yaml
kubernetes/search-engine/values.yaml
```

Настроены три Gitlab-окружения review/<имя git-ветки>, staging и production.
URL на развернутый сервис в каждом окружении статичен, т.е. нужно вручную прописывать полученный временный IP для ingress 
каждого окружения в DNS. 

## Настроить мониторинг (сбор метрик, алертинг, визуализация)

Настроен мониторинг средствами prometheus с визуализацией grafana.
Установка произведена в namespace default.
Дашборды загружаются из директории dashboards. 

Установлен сервис:
```bash
$ helm dep update kubernetes/monitoring/
$ helm install kubernetes/monitoring/ -n monitoring
```
