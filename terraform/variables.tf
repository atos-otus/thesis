variable "gitlab_db_password" {
  description = "Password for the GitLab Postgres user"
}

variable "project_id" {
  description = "The project ID to deploy to"
}

variable "region" {
  description = "GCP region to deploy resources to"
}
