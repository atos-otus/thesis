terraform {
  required_version = ">=0.11,<0.12"
    backend "gcs" {
      bucket = "tfstate-otus-thesis-245613"
      prefix = "otus-thesis"
    }
}
