provider "google" {
  project = "${var.project_id}"
  region  = "${var.region}"
}

module "gke-gitlab" {
  source = "modules/terraform-google-gke-gitlab"
  certmanager_email = "no-reply@linux.technology"
  gitlab_db_password = "${var.gitlab_db_password}"
  project_id = "${var.project_id}"
  region = "${var.region}"
}

//resource "google_compute_global_address" "review" {
//  name = "review"
//}
//
//resource "google_compute_global_address" "stage" {
//  name = "stage"
//}
//
//resource "google_compute_global_address" "prod" {
//  name = "prod"
//}