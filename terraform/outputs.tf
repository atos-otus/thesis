output "gitlab_url" {
  value = "${module.gke-gitlab.gitlab_url}"
}

output "root_password_instructions" {
  value = "${module.gke-gitlab.root_password_instructions}"
}

//output "review_ip" {
//  value = "${google_compute_global_address.review.address}"
//}
//
//output "stage_ip" {
//  value = "${google_compute_global_address.stage.address}"
//}
//
//output "prod_ip" {
//  value = "${google_compute_global_address.prod.address}"
//}
